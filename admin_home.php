<?php
include("dbconnect.php");
session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
	}
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 
include("functions.php");
  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
                <ul class="list-group">
                        <li class='list-group-item'>Videos
                                <span class='badge'><?php echo countRecord("SELECT * FROM article",$db); ?></span>
                            </li>
                             <li class='list-group-item'>Requests
                                <span class='badge'><?php echo countRecord("SELECT * FROM request",$db); ?></span>
                            </li>
                             <li class='list-group-item'>Subscribers
                                <span class='badge'><?php echo countRecord("SELECT * FROM subscribers",$db); ?></span>
                            </li>
                             <li class='list-group-item'>Admins
                                <span class='badge'><?php echo countRecord("SELECT * FROM admin",$db); ?></span>
                            </li>
                            <br>
                            <h4 class="list-group-heading">Categories</h4>
                            <hr>
                            <br>
                        <?php
                        $sql="SELECT * FROM categoriestable";
                            $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $cate=$row["CATEGOR"];
                            $cout=countRecord("SELECT * FROM article WHERE CATE='$cate'",$db);
                            if ($cout!=0) {
                                $count=$cout;
                            }
                            else{
                                $count=0;
                            }
                            echo " <li class='list-group-item'>$cate
                                <span class='badge'>$cout</span>
                            </li>";
                        }
                    }
                    else {
                       echo' <li class="list-group-item">No Record Found</li>';
                    }
                        ?>


                        </ul>
                    

                </div>
            <div class="col-sm-2"></div>

</div>
</div>

  
</body>


</html>
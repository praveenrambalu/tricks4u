<?php
include("dbconnect.php");
include("functions.php");
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; ?>
 
</head>

<body>
  <?php include "nav.php"; ?>
    <div class="container-fluid fs">
        <!-- container starts -->
        <div class="row ">
            <!-- main row -->
            <div class="col-sm-1"></div>
            <div class="col-sm-6">
                <!-- left part -->
                <div class="heading">
                    <h3 class="headingText">Latest Article</h3>
                </div>
                <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8">
                                <form action="<?php echo $_SERVER['PHP_SELF']?>" class="fss" method="post" autocomplete="off">
                                    <div class="form-group">
                                        <label for="name">Name: </label>
                                        <input type="text" name="name" required id="" placeholder="Enter your Name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="mail">Email: </label>
                                        <input type="email" name="mail" required id="" placeholder="Enter your Email id" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="video">Video: </label>
                                        <textarea type="text" name="video" required id="" placeholder="Say about the video which you want to upload.? " class="form-control" style="resize: none;"></textarea>
                                    </div>

                                    <input type="submit" value="Submit" name="rqsubmit" class="btn btn-block btn-primary">
                                </form>
                                <?php
                               if (isset($_POST["rqsubmit"])) {
                                   $name=$_POST["name"];
                                   $usermail=$_POST["mail"];
                                   $video=$_POST["video"];

                                   $sql = "INSERT INTO request ( NAME, EMAIL, VIDEOREQ) VALUES ('$name','$usermail','$video')";
                                if($db->query($sql))
				                    {
                                        echo '<script>swal("Good Job..!","Your Request  has been Sent.!","success");</script>';
                                        $to='praveenrambalu@gmail.com' ;             // Receiver Email ID, Replace with your email ID
                                    $subject='New Video Request for Tricks4U Tamil';
                                    $message="\n"."New Request from :".$name."\n video is :  ". $video ."\n Thank you";
                                    $header="From: tricks4utamil@gmail.com";
                                    $retval = mail ($to,$subject,$message,$header);
                                    // echo $retval;
                                    $sql="SELECT * FROM subscribers WHERE MAIL='$usermail';";
                                    $res=$db->query($sql);
        //echo $res->num_rows;
                                        if($res->num_rows>0)
                                        {
                                                    // echo '<script>swal("Sorry!", "Your Mail already Exists! you will get the notification while we upload a new video.", "warning");</script>';
                                        }
                                        else{
                                                $sql = "INSERT INTO subscribers ( MAIL) VALUES ('{$usermail}');";
                                                 $db->query($sql);

                                        }
				                        }
				                        else
                                        {
                                        echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
                                        
                                        }

                               }
                               ?>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>

                
                <!-- left part ends  -->
            </div>
            <div class="col-sm-1"></div>

       <?php include "rightpart.php";?>
       
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <h6 class="footerNewsletter">Register for new Updates</h6>
                    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" class="footerForm">
                        <div class="input-group ">
                            <input type="email" class="form-control footerFormInput" required placeholder="Enter email here" id="email" name="email">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit" name="submit">
                                <i class="fa fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                                      <?php include"sub.php"; ?>

                </div>
                <div class="col-sm-4">
                    <img src="img/logo.png" class="img-responsive">
                    <h1 class="footerLogoText">Tricks4U Tamil</h1>

                </div>
                <div class="col-sm-4 text-center">
                    <h2>About Us</h2>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <p class="text-justify">
                            <span class="prime">Tricks4U Tamil </span>- நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel .
                        </p>
                        <p class="text-bold">Contact Us : <a href="mailto:tricks4utamil@gmail.com">tricks4utamil@gmail.com</a></p>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
    </footer>
  
</body>


</html>
<?php
include("dbconnect.php");
session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
	}
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 
include("functions.php");
  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
                <form action="<?php echo $_SERVER['PHP_SELF']?>" class="fss" method="post" autocomplete="off">
                                    <h6>Add Category (Required)</h6>
                                    <hr>
                                 
                                    <div class="form-group">
                                        <label for="category">Category: </label>
                                        <input type="text" name="category" required id="" placeholder="ex : Ethical Hacking" class="form-control">
                                    </div>
                                    <input type="submit" value="Add" name="submit" class="btn btn-block btn-primary">

                    
                    </form>
                    <?php
                    if (isset($_POST["submit"])) {
                        $category=$_POST["category"];
                        $sql="SELECT * FROM categoriestable WHERE CATEGOR='$category';";
                        $res=$db->query($sql);
		// echo $res->num_rows;
			if($res->num_rows>0)
			 {
				echo '<script>swal("Sorry!", "The Category  '.$category.'  already available", "warning");</script>';
			 }
			else{

                $sql = "INSERT INTO categoriestable ( CATEGOR) VALUES ('{$category}');";
                if($db->query($sql))
				{
				echo '<script>swal("Good Job..!","The Category '.$category.' added ..","success");</script>';
				}
				else
				{
				echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				
				}
            }
                    }
                    ?>
                    <hr>
                    <?php
                        if(isset($_GET["mes"])){
                            echo $_GET["mes"];
                        }
                    ?>
                        <div class="table-responsive fss">
                            <table class="table table-striped ">
                            
                            <thead>
                                <tr>
                                <th class="text-center"> S.no</th>
                                <th class="text-center"> Category </th>
                                <th class="text-center"> Edit </th>
                                <th class="text-center"> Delete </th>
                                </tr>
                            </thead>
                            
                            <tbody class="text-center">

                            <?php 
                                $sql="SELECT * FROM categoriestable";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                        {
                                            $i=1;
                                            while($row=$res->fetch_assoc())
                                        {
                                            $cid=$row["CID"];
                                            $category=$row["CATEGOR"];
                                            echo "<tr>
                                                    <td>$i</td>
                                                    <td>$category</td>
                                                    <td><a href='edit_cat.php?cid=$cid' class='btn btn-edit'><span class='fa fa-edit'></span></a></td>
                                                    <td><a href='del_cat.php?cid=$cid' class='btn btn-del'><span class='fa fa-trash'></span></a></td>
                                                    
                                                    </tr>";
$i++;
                                        }
                                    }
                            ?>
                                
                            </tbody>
                            
                            </table>
                     </div>




                </div>
            <div class="col-sm-2"></div>

</div>
</div>

  
</body>


</html>
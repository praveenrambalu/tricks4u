<?php
include("dbconnect.php");
session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
	}
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 
include("functions.php");
  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
             <?php
                        if (isset($_POST["submit"])) {
                           $title=$_POST["title"];
                           $category=$_POST["cate"];
                           $description=$_POST["description"];
                           $thumbnail=$_POST["thumb"];
                           $youtube=$_POST["youtube"];
                           $embededcodepen=$_POST["embededcodepen"];
                           $embededjsfiddle=$_POST["embededjsfiddle"];
                           $gdrive=$_POST["gdrive"];
                           $github=$_POST["github"];
                           $codepen=$_POST["codepen"];
                           $jsfiddle=$_POST["jsfiddle"];
                         


                           $sql = "INSERT INTO article ( THUMB, CATE, TITLE, DESCR, YOUTUBE, EMBEDEDCODEPEN, EMBEDEDJSFIDDLE, GOOGLE, GITHUB, CODEPEN, JSFIDDLE) VALUES ('$thumbnail', '$category', '$title', '$description', '$youtube', '$embededcodepen', '$embededjsfiddle', '$gdrive', '$github', '$codepen', '$jsfiddle')";
                            if($db->query($sql))
				                    {
                                        echo '<script>swal("Good Job..!","Video has been Uploaded.!","success");</script>';
                                    }
                                    else{
                                        echo '<script>swal("Sorry..!","Some error occured , Please try after sometime","error");</script>';
                                    
                                    }
                        }
                    ?>
                <form action="<?php echo $_SERVER['PHP_SELF']?>" class="fss" method="post" autocomplete="off">
                                    <h6>Video Details (Required)</h6>
                                    <hr>
                                 
                                    <div class="form-group">
                                        <label for="title">Title: </label>
                                        <input type="text" name="title" required id="" placeholder="TITLE" class="form-control">
                                    </div>
                                         <div class="form-group">
                                        <label for="cate">Category: </label>
                                        <select name="cate"  required  class="form-control text-black">
                                    <option></option>
                                    <?php
                                    $sql="SELECT * FROM categoriestable";
                                          $res=$db->query($sql);
                                        if($res->num_rows>0)
                                                {
                                                    while($row=$res->fetch_assoc())
                                                {
                                                    $cate=$row["CATEGOR"];
                                                echo "<option value='$cate'>$cate</option>";

                                                }
                                            }
                                            else{
                                                echo '<option>----No Category Found---</option>';
                                            }
                                                            ?>
                                    </select> </div>
                                     <div class="form-group">
                                        <label for="description">Desc: </label>
                                        <textarea type="text" style="resize:none;" name="description" required id="" placeholder="Description" class="form-control"></textarea>
                                    </div>
                                     <div class="form-group">
                                        <label for="thumb">Thumbnail: </label>
                                        <input type="url" name="thumb" required id="" placeholder="http://example.com/exmplae.png" class="form-control">
                                    </div>
                                      <div class="form-group">
                                        <label for="youtube">Youtube: </label>
                                        <input type="text" name="youtube" required id="" placeholder="ex: gpI33upvbuM" class="form-control">
                                    </div>
                                    <hr>
                                    <h6>Embeded Sources (optional)</h6>
                                    <hr>
                                         <div class="form-group">
                                        <label for="embededcodepen">Codepen: </label>
                                        <input type="text" name="embededcodepen"   id="" placeholder="ex: pKKYqB" class="form-control">
                                    </div>
                                         <div class="form-group">
                                        <label for="embededjsfiddle">jsfiddle: </label>
                                        <input type="text" name="embededjsfiddle"  id="" placeholder="ex: /bqyfvshc/10" class="form-control">
                                    </div>
                                    <hr>
                                    <h6>Downloadable Sources (optional)</h6>
                                    <hr>
                                          <div class="form-group">
                                        <label for="gdrive">G-Drive: </label>
                                        <input type="text" name="gdrive"  id="" placeholder="ex:https://drive.google.com/open?id=1iiHiOd4dkSSz72IrdURXViCD " class="form-control">
                                    </div>
                                         <div class="form-group">
                                        <label for="github">GitHub: </label>
                                        <input type="text" name="github"  id="" placeholder="ex: https://github.com/tricks4u/repositoryname" class="form-control">
                                    </div>
                                     <div class="form-group">
                                        <label for="codepen">codepen: </label>
                                        <input type="text" name="codepen"  id="" placeholder="ex: https://codepen.io/tricks4u/pen/wXXZNw" class="form-control">
                                    </div> <div class="form-group">
                                        <label for="jsfiddle">jsfiddle: </label>
                                        <input type="text" name="jsfiddle"  id="" placeholder="ex: https://jsfiddle.net/tricks4u/89fu8/1/" class="form-control">
                                    </div>
                          

                                 

                                    <input type="submit" value="Submit" name="submit" class="btn btn-block btn-primary">
                                </form>
                   

          </div>
            <div class="col-sm-2"></div>

</div>
</div>
<div class="fs"></div>
  
</body>


</html>
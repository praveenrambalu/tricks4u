<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["REGNO"]))
	{
		header('Location:index.php');
    }
    else{
        $reg=$_SESSION['REGNO'];
        $sql="SELECT * FROM student WHERE REGNO=$reg";
          $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                             while($row=$res->fetch_assoc())
                        {
                          $sid=$row["ID"];
                          $name=$row["NAME"];
                          $regno=$row["REGNO"];
                          $round=$row["ROUND"];
                        }
                    }
                    

                    if($round!='TECH2'){
                    header("Location:index.php?war=Test not available for you please contact the admin");
                    }
                    

                    $sql="SELECT * FROM result WHERE ROLLNO=$regno";
                        $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                             while($row=$res->fetch_assoc())
                        {
                            $TECH2=$row["TECH2"];
                        }
                        if ($TECH2!=NULL) {
                    header("Location:index.php?war=You have completed the Test2. any doubt contact the admin");
                            # code...
                        }
                    }
                    
    }
?>
<!DOCTYPE html>
<html>

<head>
   <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    <h3 class="dept-title">Department of Computer Science and Engineering</h3>
    <h4 class="text-center round-title">Technical Question - Round 2</h4>
    <h5 class="text-center">Welcome <?php echo "<span class='text-uppercase'>$name - </span> ( $regno )"?></h5>
<div class="times"></div>


    <div class="container">
        <div class="row fs">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">

                <form class="form" action="" method="POST">
                    <?php
                    $sql="SELECT * FROM question WHERE ROUNDS='TECH2' LIMIT 0,20";
                       $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $i=1;
                             while($row=$res->fetch_assoc())
                        {
                            $que=$row["QUESTION"];
                            $opa=$row["OPTIONA"];
                            $opb=$row["OPTIONB"];
                            $opc=$row["OPTIONC"];
                            $opd=$row["OPTIOND"];
                            $qid=$row["QID"];
                            $ans1=$row["CORRECT"];
                   echo" <div class='form-group'>
                         <div class='qs'>$i.  $que</div>
                        <div class='ans'>
                            
                            <br>
                            <input type='radio' name='op$i' value='A' id=''>$opa<br>
                            <input type='radio' name='op$i' value='B' id=''>$opb<br>
                            <input type='radio' name='op$i' value='C' id=''>$opc<br>
                            <input type='radio' name='op$i' value='D' id=''>$opd
                        </div>
                    </div>";
                    $i++;
                         }
                    }

                    ?>

                    <input type="submit" name="submit" value="Submit" class="btn btn-info ansSub">
                </form>
<?php
if (isset($_POST["submit"])) {
    // $a1=$_POST["op1"];
    // $a2=$_POST["op2"];
    // $a3=$_POST["op3"];
    // $a4=$_POST["op4"];
    // $a5=$_POST["op5"];
    // $a6=$_POST["op6"];
    // $a7=$_POST["op7"];
    // $a8=$_POST["op8"];
    // $a9=$_POST["op9"];
    // $a10=$_POST["op10"];
    // $a11=$_POST["op11"];
    // $a12=$_POST["op12"];
    // $a13=$_POST["op13"];
    // $a14=$_POST["op14"];
    // $a15=$_POST["op15"];

                    $sql="SELECT * FROM question WHERE ROUNDS='TECH2' LIMIT 0,20";
                       $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $op=1;
                            $ans=0;
                             while($row=$res->fetch_assoc())
                        {
                             $crct=$row["CORRECT"];
                            //  ECHO $crct; 
                             if ($_POST["op$op"]==$crct) {
                                $ans++;
                             }  
                             $op++;              
                        }



                            $sql="SELECT * FROM result WHERE ROLLNO='$regno' AND TECH2=NULL;";
                          $res=$db->query($sql);
		// echo $res->num_rows;
                            if($res->num_rows>0)
                            {
                                echo '<script>swal("Sorry!", "The Student has completed  the Test 2", "warning");</script>';
                               
                            }
                            else{
                                
                                  $sql = "UPDATE result SET TECH2 = $ans WHERE result.ROLLNO = $regno";
                                    if($db->query($sql))
                                {
                                echo '<script>swal("Good Job..!","The Round 2 completed.. wait for the result ..","success");</script>';
                                echo "<script>window.open('index.php','_self')</script>";
                                }
                                else
                                {
                                echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
                                
                                }
                             }
                


                      
                    }
      
   
}
?>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
    <br>    <br>    <br>    <br>
</body>
<script>
$(document).ready(function(){
    countdown();
function countdown() {
    

     var a=600000;
var myVar= setInterval(function(){ 
     a=a-1000;
      var minutes = Math.floor((a % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((a % (1000 * 60 * 60)) / 1000);
    $(".times").html("<b>"+minutes+"</b> m  <b>"+seconds+"</b> s");
    if(a==0)
    {
       swal("Time Out..!","Timeout of your Test Please Contact Praveenram","warning");
       clearInterval(myVar);
       setTimeout(() => {
           window.open('index.php?war="Timed Out of your Second test Please contact the Admin"','_self')
       }, 2000);
    }
  }, 1000);
}
});
</script>
</html>
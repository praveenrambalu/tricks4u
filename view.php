<?php
include("dbconnect.php");
include("functions.php");
?><!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; ?>
<style>
#comments iframe {
    width: 90% !important;
}
#comments{
    max-width:250px !important;
    overflow:auto;
}
</style>
</head>
<?php
if (isset($_GET["id"])) {
    $pid=$_GET["id"];
}
else{
    $pid=1;
}
?>

<?php
$sql="SELECT * FROM article WHERE ID=$pid";
 $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $id=$row["ID"];
                            $title=$row["TITLE"];
                            $video=$row["YOUTUBE"];
                            $embededcodepen=$row["EMBEDEDCODEPEN"];
                            $embededjsfiddle=$row["EMBEDEDJSFIDDLE"];
                            $git=$row["GITHUB"];
                            $drive=$row["GOOGLE"];
                            $jsfiddle=$row["JSFIDDLE"];
                            $codepen=$row["CODEPEN"];
                            $descr=$row["DESCR"];
                            $cate=$row["CATE"];
                            $log=$row["LOG"];

                        }
                    }
                    else{
                 echo "<script>window.open('index.php','_self')</script>";
                        // header("Location:index.php");
                    }

?>
<body>
     <?php include "nav.php"; ?>

    <div class="container-fluid fs">
        <!-- container starts -->
        <div class="row ">
            <!-- main row -->
            <div class="col-sm-1"></div>
            <div class="col-sm-6">
                <!-- left part -->

                <div class="heading">
                    <h3 class="headingText">Video</h3>
                </div>
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <h4 class="videoHeading text-justify text-center">
                            <?php echo $title ?>
                        </h4>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- video div starts -->
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="video text-center">
                            <iframe height="315" src="https://www.youtube.com/embed/<?php echo $video; ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="share text-center fss">
                            <a href="https://www.facebook.com/sharer.php?t=Tricks4U%20Tamil <?php echo $title ?>&u=http://tricks4utamil.tk/view.php?id=<?php echo $id ?>" target="_blank" class="btn btn-social">
                                <span class="fa fa-facebook"></span>
                            </a>
                            <a href="https://twitter.com/intent/tweet?text=Tricks4U%20Tamil <?php echo $title ?>&url=https://tricks4utamil.tk/view.php?id=<?php echo $id ?>" target="_blank" class="btn btn-social">
                                <span class="fa fa-twitter"></span>
                            </a>
                            <a href="https://www.linkedin.com/shareArticle?title=Tricks4U%20Tamil<?php echo $title ?>&url=https://tricks4utamil.tk/view.php?id=<?php echo $id ?>" target="_blank" class="btn btn-social">
                                <span class="fa fa-linkedin"></span>
                            </a>
                            <a href="whatsapp://send?text=<?php echo $title ?> http://tricks4utamil.tk/view.php?id=<?php echo $id ?>" target="_blank" class="btn btn-social btn-whatsapp">
                                <span class="fa fa-whatsapp"></span>
                            </a>
                            <a href="mailto:?to=&subject=Tricks4u Tamil&body=<?php echo $title ?>\n  Check this View on the below link \n  https://tricks4utamil.tk/view.php?id=<?php echo $id ?>" target="_blank" class="btn btn-social">
                                <span class="fa fa-envelope"></span>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- video div ends -->
                <div class="heading">
                    <h3 class="headingText">Description</h3>
                </div>
                <div class="row">
                    <!-- description div starts -->
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="description fss">
                            <p class="text-justify">
                               <?php echo $descr ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- description div ends -->
                <div class="heading fss">
                    <h3 class="headingText">Related Videos</h3>
                </div>
                <div class="row">
                    <!--relative div starts-->
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="relatedVideos fss text-center">
                            <?php
                            $sql="SELECT * FROM article WHERE CATE='$cate' LIMIT 3" ;
                            
                            $res=$db->query($sql);
                             if($res->num_rows>0)
                            {
                            while($row=$res->fetch_assoc())
                                {
                                $thumb=$row["THUMB"];
                                $head=$row["TITLE"];
                                $viewid=$row["ID"];
                                echo "<div class='relatedBox'>
                                <a href='view.php?id=$viewid'><img src='$thumb' alt='' class='img img-thumbnailown'></a>
                                <h6>$head</h6>
                            </div>";
                                }
                             }
                            else {echo"<h6>No Related Videos found yet</h6>";}
                            ?>
                            
                            

                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- relative div end-->
                <div class="heading fss">
                    <h3 class="headingText">Code</h3>
                </div>
                <div class="row fss">
                    <!--source  div starts-->
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-center">

                    <?php
                        if($embededcodepen!=""){

                            echo "<p data-height='265' data-theme-id='0' data-slug-hash='$embededcodepen' data-default-tab='html,result' data-user='tricks4u' data-embed-version='2' class='codepen'></p>";
                        }
                        else if($embededjsfiddle!=""){
                            echo "<script async src='//jsfiddle.net/tricks4u$embededjsfiddle/embed/js,html,css,result/dark/'></script>";
                        }
                        else{
                            echo "<h6>No Code snippet found for this video..!</h6>";
                        }
                    ?>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- source div end-->




                <div class="heading fss">
                    <h3 class="headingText">Download Source </h3>
                </div>
                <div class="row fss">
                    <!--download div starts-->
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-center">
                        <a href="<?php echo $drive; ?>" class="btn btn-sourceGoogle"><span class="fa fa-google"></span></a>
                        <a href="<?php echo $git; ?> " class="btn btn-sourceGithub"><span class="fa fa-github"></span></a>
                        <a href="<?php echo $codepen; ?> " class="btn btn-sourceCodepen"><span class="fa fa-codepen"></span></a>
                        <a href="<?php echo $jsfiddle; ?> " class="btn btn-sourceJsfiddle"><span class="fa fa-jsfiddle"></span></a>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- download div end-->


                <div class="heading fss">
                    <h3 class="headingText">Comments</h3>
                </div>
                <div class="row fss">
                    <!--comment div starts-->
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
<div id="comments"></div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- comment div end-->

                <script>
                </script>







                <!-- left part ends  -->
            </div>
            <div class="col-sm-1"></div>

                <?php include "rightpart.php";?>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <h6 class="footerNewsletter">Register for new Updates</h6>
                    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" class="footerForm">
                        <div class="input-group ">
                            <input type="email" class="form-control footerFormInput" required placeholder="Enter email here" id="email" name="email">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit" name="submit">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                            </div>
                        </div>
                    </form>
                    <?php include"sub.php"; ?>
                </div>
                <div class="col-sm-4">
                    <img src="img/logo.png" class="img-responsive">
                    <h1 class="footerLogoText">Tricks4U Tamil</h1>

                </div>
                <div class="col-sm-4 text-center">
                    <h2>About Us</h2>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <p class="text-justify">
                            <span class="prime">Tricks4U Tamil </span>- நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel .
                        </p>
                        <p class="text-bold">Contact Us :
                            <a href="mailto:tricks4utamil@gmail.com">tricks4utamil@gmail.com</a>
                        </p>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
    </footer>

</body>
<script>
  gapi.comments.render('comments', {
        href: 'http://tricks4utamil.tk/view.php?id=1',
        width: 'auto',
        first_party_property: 'BLOGGER',
        view_type: 'FILTERED_POSTMOD'
    });
</script>
</html>
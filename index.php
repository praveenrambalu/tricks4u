<?php
include("dbconnect.php");
include("functions.php");
if (isset($_GET["page"])) {
    $page=$_GET["page"];
    $limit=($page*10)-10;

}
else{
    $page=1;
    $limit=0;
}
 $prev=$page-1;
$next=$page+1;
 if($prev<=0){
 $prev=1;
 }
 $nextpage=" <li class=''><a href='index.php?page=$next'>Next   <span class='fa fa-arrow-right'></span> </a></li>";
 if($page==1){
      $previous="";
 }
 else{
      $previous=" <li class=''><a href='index.php?page=$prev'><span class='fa fa-arrow-left'></span>  Previous</a></li>";

 }

?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; ?>
 
</head>

<body>
  <?php include "nav.php"; ?>
    <div class="container-fluid fs">
        <!-- container starts -->
        <div class="row ">
            <!-- main row -->
            <div class="col-sm-1"></div>
            <div class="col-sm-6">
                <!-- left part -->
                <div class="heading">
                    <h3 class="headingText">Latest Article</h3>
                </div>


                <div class="list-group fss">
                    <?php
                        $sql="SELECT * FROM article ORDER BY  ID DESC LIMIT $limit,10";
                        $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $id=$row["ID"];
                            $img=$row["THUMB"];
                            $title=$row["TITLE"];
                            $descr=$row["DESCR"];
                            $log=time_elapsed_string($row["LOG"]);
                           echo "<a href='view.php?id=$id' class='list-group-item list-group-item-action flex-column align-items-start '>
                             <div class='d-flex w-100 justify-content-between'>
                            <img src='$img' class='img img-thumbnail img-responsive'>
                            <h5 class='mb-1 headingText'>$title</h5>
                            <small> $log</small>
                        </div>
                        <p class='mb-1'>$descr</p>
                               
                    </a>";
                        }
                        
                          echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                            echo $nextpage;
                             echo "</ul> </div>";
                        }
                        else{
                            echo "<a href='#' class='list-group-item list-group-item-action flex-column align-items-start '>
                             <div class='d-flex w-100 justify-content-between'>
                            <h5 class='mb-1 headingText'>Sorry No Record Found..!</h5>
                            </div></a>";
                             echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                           
                             echo "</ul> </div>";
                        }
                                            
                    ?>
                   
                   

                </div>
                <!-- left part ends  -->
            </div>
            <div class="col-sm-1"></div>

       <?php include "rightpart.php";?>
       
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <h6 class="footerNewsletter">Register for new Updates</h6>
                    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" class="footerForm">
                        <div class="input-group ">
                            <input type="email" class="form-control footerFormInput" required placeholder="Enter email here" id="email" name="email">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit" name="submit">
                                <i class="fa fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                                        <?php include"sub.php"; ?>

                </div>
                <div class="col-sm-4">
                    <img src="img/logo.png" class="img-responsive">
                    <h1 class="footerLogoText">Tricks4U Tamil</h1>

                </div>
                <div class="col-sm-4 text-center">
                    <h2>About Us</h2>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <p class="text-justify">
                            <span class="prime">Tricks4U Tamil </span>- நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel .
                        </p>
                        <p class="text-bold">Contact Us : <a href="mailto:tricks4utamil@gmail.com">tricks4utamil@gmail.com</a></p>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
    </footer>
  
</body>


</html>
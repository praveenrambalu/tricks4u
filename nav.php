  <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png" class=" navbar-brand">
                    <h1 class="navbar-brand">Tricks4U Tamil</h1>
                </a>
            </div>


            <div class="collapse navbar-collapse" id="nav">
                <ul class="nav navbar-nav navbar-right text-uppercase">
                    <li class="">
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="request.php">Request</a>
                    </li>
                    <li>
                        <a href="admin.php">Admin</a>
                    </li>

                    <li class="formBox">
                        <form class="navbar-form navbar-left" action="search.php" method="Get">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" id="search" name="search">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                    </button>

                                </div>
                            </div>
                        </form>
                    </li>
                    <li>
                        <button class="btn navSearchBtn"><i class="fa fa-search" id="searchKey"></i> </button>
                    </li>

                </ul>


            </div>
        </div>

    </nav>
    <!-- nav ends -->
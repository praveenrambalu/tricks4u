<?php
include("dbconnect.php");

session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
    }
if (isset($_GET["page"])) {
    $page=$_GET["page"];
    $limit=($page*10)-10;

}
else{
    $page=1;
    $limit=0;
}
 $prev=$page-1;
$next=$page+1;
 if($prev<=0){
 $prev=1;
 }
 $nextpage=" <li class=''><a href='view_all_video.php?page=$next'>Next   <span class='fa fa-arrow-right'></span> </a></li>";
 if($page==1){
      $previous="";
 }
 else{
      $previous=" <li class=''><a href='view_all_video.php?page=$prev'><span class='fa fa-arrow-left'></span>  Previous</a></li>";

 }

?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 
include("functions.php");
  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
                <ul class="list-group fss">
                        <li class='list-group-item'>Total Videos
                                <span class='badge'><?php echo countRecord("SELECT * FROM article",$db); ?></span>
                            </li>
                             

                        </ul>
                         <hr>
                    <?php
                        if(isset($_GET["mes"])){
                            echo $_GET["mes"];
                        }
                    ?>
                    <hr>
                    
                    <?php
                        $sql="SELECT * FROM article ORDER BY  ID DESC LIMIT $limit,10";
                        $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $id=$row["ID"];
                            $img=$row["THUMB"];
                            $title=$row["TITLE"];
                            $descr=$row["DESCR"];
                            $log=time_elapsed_string($row["LOG"]);
                           echo "<div class='list-group-item list-group-item-action flex-column align-items-start '>
                        <a href='view.php?id=$id' class='list-group-item list-group-item-action flex-column align-items-start '>
                            <div class='d-flex w-100 justify-content-between'>
                                <img src='$img' class='img img-thumbnail img-responsive'>
                                <h5 class='mb-1 headingText'>$title</h5>
                                <small> $log</small>
                            </div>
                            <p class='mb-1'>$descr</p>
                        </a>
                        <div class=' btn-group text-center editdelete'>
                            <a href='edit_vid.php?vid=$id' class='btn btn-edit'>
                                <span class='fa fa-edit'></span>
                            </a>
                            <a href='del_vid.php?vid=$id' class='btn btn-del'>
                                <span class='fa fa-trash'></span>
                            </a>
                        </div>
                    </div>";
                        }
                    
                          echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                            echo $nextpage;
                             echo "</ul> </div>";
                }
                        else{
                            echo "<a href='#' class='list-group-item list-group-item-action flex-column align-items-start '>
                             <div class='d-flex w-100 justify-content-between'>
                           
                            <h5 class='mb-1 headingText'>Sorry No Record Found..!</h5>
                            </div></a>";
                             echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                           
                             echo "</ul> </div>";
                        }
                                            
                    
                    ?>
                   
                   

              
                    </div>
            <div class="col-sm-2"></div>

</div>
</div>

  
</body>


</html>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no ,maximum-scale=1.0, user-scalable=no">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Tricks4U Tamil - நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel ." />
    <meta name="keywords" content="praveenram ,balachandran ,praveenrambalu,praveenram balachandran,snapstylers,Tricks4U Tamil - நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel .,web,designer,web designer, web developer" />
    <meta name="author" content="Praveenram Balachandran" />
    <title>Trick4U Tamil</title>
    <link rel="shortcut icon" href="img/favicon.ico">



    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="Tricks4U Tamil" />
    <meta property="og:image" content="http://tricks4utamil.tk/img/whatsappshare.png" />
    <meta property="og:url" content="http://tricks4utamil.tk" />
    <meta property="og:site_name" content="tricks4u tamil" />
    <meta property="og:description" content="Tricks4U Tamil - நம் தமிழ் மொழியில் . This is the Official Website of Tricks4u Tamil YouTube Channel ." />
    <meta name="twitter:title" content="Tricks4u Tamil" />
    <meta name="twitter:image" content="http://tricks4utamil.tk/img/whatsappshare.png" />
    <meta name="twitter:url" content="http://tricks4utamil.tk" />



    <!-- <link rel="stylesheet" href="css/materialize.css"> -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Allerta Stencil" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="css/admin.css">

    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/pace.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

    <script src="js/bootstrap.js"></script>
    <script src="js/main.js"></script>
    <script src="js/pace.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <!-- <script src="js/materialize.js"></script> -->

    <!-- <script src="js/main.js"></script> -->
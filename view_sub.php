<?php
include("dbconnect.php");
session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
    }
    if (isset($_GET["page"])) {
    $page=$_GET["page"];
    $limit=($page*10)-10;

}
else{
    $page=1;
    $limit=0;
}
 $prev=$page-1;
$next=$page+1;
 if($prev<=0){
 $prev=1;
 }
 $nextpage=" <li class=''><a href='view_sub.php?page=$next'>Next   <span class='fa fa-arrow-right'></span> </a></li>";
 if($page==1){
      $previous="";
 }
 else{
      $previous=" <li class=''><a href='view_sub.php?page=$prev'><span class='fa fa-arrow-left'></span>  Previous</a></li>";

 }
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 
include("functions.php");
  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
                <ul class="list-group">
                      
                             <li class='list-group-item'>Total Subscribers
                                <span class='badge'><?php echo countRecord("SELECT * FROM subscribers",$db); ?></span>
                            </li>
                             <li class='list-group-item'>
                                 <a href="mailer.php" class="btn btn-block btn-primary"><span class="fa fa-envelope"></span> Send Mail</a>
                                
                            </li>
                     <hr>
                     <h6>Subscribers</h6>     
                     <hr>
                            
                        <?php
                        $sql="SELECT * FROM subscribers ORDER BY ID DESC LIMIT $limit,10";
                            $res=$db->query($sql);
                    if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                           $id=$row["ID"];
                           $mail=$row["MAIL"];
                           $status=$row["STATUS"];
                                                     $log=time_elapsed_string($row["LOG"]);
                            echo "<a href='#' class='list-group-item'>
      <h4 class='list-group-item-heading'>$mail <small class='pull-right'>$log</small></h4>
	  <span class='requestmail'>$status</span>
    </a>";

                        }
                         echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                            echo $nextpage;
                             echo "</ul> </div>";
                    }
                    else {
                       echo' <li class="list-group-item">No Record Found</li>';
                         echo "<div class='text-center fss'> <ul class='pager'>";
                            echo $previous;
                           
                             echo "</ul> </div>";
                    }
                        ?>


                        </ul>
                    

                </div>
            <div class="col-sm-2"></div>

</div>
</div>

  
</body>


</html>
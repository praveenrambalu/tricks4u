     <div class="col-sm-3">
                <!-- Major right part starts -->
                <div class="heading">
                    <h3 class="headingText">Tricks4U</h3>
                </div>
                <div class="row fss">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-center">
                        <div class="g-ytsubscribe" data-channelid="UCKaxhwrCbX-sEvdQqdtXcjQ" data-layout="full" data-count="default"></div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>


                <div class="heading fss">
                    <h3 class="headingText">Join Us</h3>
                </div>
                <div class=" row fss">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-center">
                        <div class="">
                            <a href="https://facebook.com/praveenrambalu" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-facebook"></span> Facebook</a>
                            <a href="https://twitter.com/praveenrambalu" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-twitter"></span> Twitter</a>
                            <a href="https://linkedin.com/in/praveenrambalu" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-linkedin"></span> LinkedIn</a>
                            <a href="https://instagram.com/praveenrambalu" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-instagram"></span> Instagram</a>
                            <a href="https://plus.google.com/104300890132201177678" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-google-plus"></span> Google Plus</a>
                            <a href="https://goo.gl/ELd5cm" class="btn btn-block btn-social pull-left">
                                <span class="pull-right fa fa-youtube-play"></span> Youtube</a>
                        </div>

                    </div>
                    <div class="col-sm-1"></div>
                </div>

                <div class="heading">
                    <h3 class="headingText">Source Codes</h3>
                </div>
                <div class="row fss">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-center">
                        <a href="https://github.com/tricks4u" class="btn btn-github ">
                            <span class="fa fa-github"></span>
                        </a>
                        <a href="https://jsfiddle.net/user/tricks4u/fiddles/" class="btn btn-jsfiddle ">
                            <span class="fa fa-jsfiddle"></span>
                        </a>
                        <a href="https://codepen.io/tricks4u/" class="btn btn-codepen ">
                            <span class="fa fa-codepen"></span>
                        </a>
                        <div class="sharethis-inline-share-buttons"></div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>

                <div class="heading">
                    <h3 class="headingText">Categories</h3>
                </div>
                <div class="row fss">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 ">
                        <ul class="list-group">

                            <?php
                        $sql="SELECT * FROM categoriestable";
                            $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $cate=$row["CATEGOR"];
                            $cout=countRecord("SELECT * FROM article WHERE CATE='$cate'",$db);
                            if ($cout!=0) {
                                $count=$cout;
                            }
                            else{
                                $count=0;
                            }
                            echo " <li class='list-group-item'>$cate
                                <span class='badge'>$cout</span>
                            </li>";
                        }
                    }
                    else {
                       echo' <li class="list-group-item">No Record Found</li>';
                    }
                        ?>


                        </ul>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <!-- major right part ends -->
            </div>
            <div class="col-sm-1"></div>
            <!-- main row ends below -->
        </div>
        <!-- Main container ends-->
    </div>
    <!-- pushpin -->
    <div class="pushpin">
        <div class="pushpinHead">
            <span class="fa fa-times-circle pull-right" id="closePushpin"></span>
        </div>
        <div class="pushpinBody text-justify">
            <img src="img/logo.png" class=" center img-responsive img-thumbnail pushpinImg">
            <div class="pushpinBodyText">
                <h4 class="pushpinTextHead">Please Subscribe our channel</h4>
                <p class="text-justify">leave us a like and comment please do share and subscribe</p>
                <a href="#" class="btn btn-secondary">Subscribe</a>
            </div>
        </div>

    </div>
    <!-- pushpin ends -->
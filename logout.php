<?php
	include "dbconnect.php";
	session_start();

	unset ($_SESSION["AID"]);
	session_destroy();
	header('Location:admin.php');
?>
    $(document).ready(function() {
        $('.carousel').carousel();
        $(".formBox").hide();
        $('.pushpin').fadeOut();
        $(".navSearchBtn").click(function() {
            $(".formBox").toggle(1000);
            $("#searchKey").toggleClass("fa fa-times");
            $("#searchKey").toggleClass("fa fa-search");
        });
        setTimeout(function() {
            $('.pushpin').fadeIn(2000);
        }, 10000);
        $("#closePushpin").click(function() {
            $(".pushpin").hide();
        });

    });
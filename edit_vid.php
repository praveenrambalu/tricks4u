<?php
include("dbconnect.php");
session_start();
if(!isset($_SESSION["AID"]))
	{
	header('Location: admin.php');
		
	}
?>
<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; 

  
  ?>
 <style>body,html{height:auto !important;}</style>
 
</head>

<?php
if (isset($_GET["vid"])) {
    $vid=$_GET["vid"];
    $sql="SELECT * FROM article WHERE ID=$vid";
         $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            while($row=$res->fetch_assoc())
                        {
                            $id=$row["ID"];
                            $title=$row["TITLE"];
                            $description=$row["DESCR"];
                            $category=$row["CATE"];
                            $youtube=$row["YOUTUBE"];
                            $thumb=$row["THUMB"];
                            $emcode=$row["EMBEDEDCODEPEN"];
                            $emjsfiddle=$row["EMBEDEDJSFIDDLE"];
                            $gdrive=$row["GOOGLE"];
                            $codepen=$row["CODEPEN"];
                            $git=$row["GITHUB"];
                            $jsfiddle=$row["JSFIDDLE"];
                            
                        }
                    } 
                    else {
	// header('Location: add_cat.php');
                       
                    }
}
else {
	// header('Location: add_cat.php');
    
}
?>

<body>
  <?php include "admin_nav.php"; ?>
    <div class="container-fluid  fs">
        <!-- container starts -->
        <div class="row fss">
            <!-- main row -->
            <div class="col-sm-2"></div>
            <div class="col-sm-8 ">
               <form action="<?php echo $_SERVER['PHP_SELF']?>" class="fss" method="post" autocomplete="off">
                                    <h6>Video Details (Required)</h6>
                                    <hr>
                                 
                                    <div class="form-group">
                                        
                                        <input type="hidden" name="id" required value="<?php echo $id; ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Title: </label>
                                        <input type="text" name="title" required value="<?php echo $title; ?>" class="form-control">
                                    </div>
                                     <div class="form-group">
                                        <label for="category">Category: </label>
                                        <input type="text" name="category" required value="<?php echo $category; ?>" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="description">Desc: </label>
                                        <textarea type="text" style="resize:none;" name="description" required  class="form-control" ><?php echo $description; ?></textarea>
                                    </div>
                                     <div class="form-group">
                                        <label for="thumb">Thumbnail: </label>
                                        <input type="url" name="thumb" required value="<?php echo $thumb; ?>" class="form-control">
                                    </div>
                                      <div class="form-group">
                                        <label for="youtube">Youtube: </label>
                                        <input type="text" name="youtube" required value="<?php echo $youtube; ?>"  class="form-control">
                                    </div>
                                    <hr>
                                    <h6>Embeded Sources (optional)</h6>
                                    <hr>
                                         <div class="form-group">
                                        <label for="embededcodepen">Codepen: </label>
                                        <input type="text" name="embededcodepen"   value="<?php echo $emcode; ?>" class="form-control">
                                    </div>
                                         <div class="form-group">
                                        <label for="embededjsfiddle">jsfiddle: </label>
                                        <input type="text" name="embededjsfiddle"  value="<?php echo $emjsfiddle; ?>" class="form-control">
                                    </div>
                                    <hr>
                                    <h6>Downloadable Sources (optional)</h6>
                                    <hr>
                                          <div class="form-group">
                                        <label for="gdrive">G-Drive: </label>
                                        <input type="text" name="gdrive" value="<?php echo $gdrive; ?>" class="form-control">
                                    </div>
                                         <div class="form-group">
                                        <label for="github">GitHub: </label>
                                        <input type="text" name="github"  value="<?php echo $git; ?>" class="form-control">
                                    </div>
                                     <div class="form-group">
                                        <label for="codepen">codepen: </label>
                                        <input type="text" name="codepen"  value="<?php echo $codepen; ?>" class="form-control">
                                    </div> <div class="form-group">
                                        <label for="jsfiddle">jsfiddle: </label>
                                        <input type="text" name="jsfiddle"  value="<?php echo $jsfiddle; ?>" class="form-control">
                                    </div>
                          
                                    <input type="submit" value="Edit" name="edit" class="btn btn-block btn-primary">

                    
                    </form>
                    <?php
                    if (isset($_POST["edit"])) {
                       $editedid=$_POST["id"];
                       $editedthumb=$_POST["thumb"];
                       $editedcategory=$_POST["category"];
                       $editedtitle=$_POST["title"];
                       $editeddescription=$_POST["description"];
                       $editedyoutube=$_POST["youtube"];
                       $editedemcodepen=$_POST["embededcodepen"];
                       $editedemjsfiddle=$_POST["embededjsfiddle"];
                       $editedgdrive=$_POST["gdrive"];
                       $editedgit=$_POST["github"];
                       $editedcodepen=$_POST["codepen"];
                       $editedjsfiddle=$_POST["jsfiddle"];

                       $sql="UPDATE article SET THUMB='$editedthumb',CATE='$editedcategory',TITLE='$editedtitle',DESCR='$editeddescription',YOUTUBE='$editedyoutube',EMBEDEDCODEPEN='$editedemcodepen',EMBEDEDJSFIDDLE='$editedemjsfiddle',GOOGLE='$editedgdrive',GITHUB='$editedgit',CODEPEN='$editedcodepen',JSFIDDLE='$editedjsfiddle' WHERE ID=$editedid;";
                    //    echo $sql;
                    if($db->query($sql))
                            {
                        //    header('Location: add_cat.php?mes="<p class="successwarning">Edited Successfull.</p>"');
                            echo "<script>window.open('view_all_video.php?mes=Edited Successful','_self')</script>";
                            }
                            else
                            {
                            echo "<script>window.open('view_all_video.php?mes=Error Occured','_self')</script>";

                        //    header('Location: add_cat.php?mes="<p class="successwarning">Some Error Occured Please try after sometime."');
                            
                            
                            }
            
                    }
                    ?>
                    <hr>
                        




                </div>
            <div class="col-sm-2"></div>

</div>
</div>

  
</body>


</html>